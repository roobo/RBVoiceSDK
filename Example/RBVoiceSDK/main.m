//
//  main.m
//  RBVoiceSDK
//
//  Created by zky_416@sina.com on 02/06/2017.
//  Copyright (c) 2017 zky_416@sina.com. All rights reserved.
//

@import UIKit;
#import "RBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RBAppDelegate class]));
    }
}
