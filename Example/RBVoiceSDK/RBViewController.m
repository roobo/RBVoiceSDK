//
//  RBViewController.m
//  RBVoiceSDK
//
//  Created by zky_416@sina.com on 02/06/2017.
//  Copyright (c) 2017 zky_416@sina.com. All rights reserved.
//

#import "RBViewController.h"
#import <RBVoiceFramework/RBVoiceHeader.h>
#import "ZYPlayVideo.h"


@interface RBViewController (){
    NSString * voicePath;
}
@property (weak, nonatomic) IBOutlet UITextField *wifiAccount;
@property (weak, nonatomic) IBOutlet UITextField *wifiPassword;
@property (weak, nonatomic) IBOutlet UITextField *userId;

@end

@implementation RBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    voicePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"voice.mp3"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)playVoiceAction:(id)sender {
    createVoiceFile(_wifiAccount.text, _wifiPassword.text, _userId.text, voicePath);
    [ZYPlayVideo playMusicURL:voicePath];
}


@end
