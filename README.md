
# RBVoiceSDK

[![CI Status](http://img.shields.io/travis/zky_416@sina.com/RBVoiceSDK.svg?style=flat)](https://travis-ci.org/zky_416@sina.com/RBVoiceSDK)
[![Version](https://img.shields.io/cocoapods/v/RBVoiceSDK.svg?style=flat)](http://cocoapods.org/pods/RBVoiceSDK)
[![License](https://img.shields.io/cocoapods/l/RBVoiceSDK.svg?style=flat)](http://cocoapods.org/pods/RBVoiceSDK)
[![Platform](https://img.shields.io/cocoapods/p/RBVoiceSDK.svg?style=flat)](http://cocoapods.org/pods/RBVoiceSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RBVoiceSDK is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RBVoiceSDK"
```

## Author

zky_416@sina.com, kieran.zhi@foxmail.com

## License

RBVoiceSDK is available under the MIT license. See the LICENSE file for more info.

