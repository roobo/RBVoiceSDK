//
//  RBVoiceHeader.h
//  RBVoiceFramework
//
//  Created by kieran on 2017/2/6.
//  Copyright © 2017年 kieran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/**
 *  缓存网络数据
 *
 *  @param name         wifi名称
 *  @param password     wifi密码
 *  @param voiceOutPath 音频生成路径
 */
int createVoiceFile(NSString * name ,NSString * password , NSString * userId ,NSString * voiceOutPath);



