
Pod::Spec.new do |s|
  s.name             = 'RBVoiceSDK'
  s.version          = '1.0.4'
  s.summary          = 'RBVoiceSDK: roobo config net work framework'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://git.oschina.net/roobo/RBVoiceSDK'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'kieran' => 'kieran.zhi@foxmail.com' }
  s.source           = { :git => 'https://git.oschina.net/roobo/RBVoiceSDK.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.vendored_frameworks = 'RBVoiceSDK/RBVoiceFramework.framework'

end
